<?php require_once 'engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library System</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  </head>
  <body style="background-color:#b5dcfc;">


    <div class="top-menu">
      <ul>
        <li><a href=''>home</a></li>
        <?php echo respectiveMenu(''); ?>
        <li><a href='cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <!-- Book holding section -->
    <section>
      <div class="container-fluid book_container">
        <h1>Welcome to The Online Library</h1>

        <div class="row array">
          <form class="ui-form" action="" method="post">
            <input type="text" name="search_value" placeholder="search book">
            <input style="display:none;"type="submit" name="search" value="search">
          </form>
          <?php searchValue(); ?>
        </div>

        <div class="row array">
          <h3>Books</h3>
          <?php getPurchaseBooks(); ?>
        </div>

        <div class="row array">
          <h3>Ebooks</h3>
          <?php getPurchaseEBooks(); ?>
        </div>
      </div>
    </section>

  </body>
</html>
