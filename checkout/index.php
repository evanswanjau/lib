<?php

require_once '../engine/infused_cogs.php';

accountSecurity('../', 'user');

if (!isset($_SESSION['cartvalues']) || $_SESSION['cartvalues'] < 1) {

  header('Location: ../');

}

# get personal information

$receipt_data = new CrudFunctionality;

$personal_info = $receipt_data->getData('*', 'users', 'email', $_COOKIE['user']);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>checkout</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <style media="screen">
      .book_purchase{
        width:19%!important;
      }
    </style>
  </head>
  <body style="background-color:#b5dcfc;">


    <div class="top-menu">
      <ul>
        <li><a href='../'>home</a></li>
        <?php echo respectiveMenu('../'); ?>
        <li><a href='../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <!-- generate receipt section -->

    <section style="padding:3%;">
      <div class="receipt container-fluid">

        <h1>RECEIPT</h1>

        <!-- PERSONAL INFORMATION -->

        <div class="row">
          <div class="step col-sm-10">
            <h4>personal information</h4>
            <p class='upper cap'><b>Full Name: </b><?php echo $personal_info[0]['full_name']; ?></p>
            <p><b>Email: </b><?php echo $personal_info[0]['email']; ?></p>
            <p><b>Phone: </b><?php echo $personal_info[0]['phone']; ?></p>
            <p class="cap"><b>Shipping Location: </b><?php echo $personal_info[0]['location']; ?></p>
          </div>
        </div>


      <!--Shopped Items-->

       <?php if (isset($_SESSION['cartvalues']) && $_SESSION['cartvalues'] > 0): ?>
         <div class="row">
           <div class="step table_design col-sm-10">
             <?php if (isset($_GET['buy_books'])): ?>
               <h4>shopped books</h4>
               <table>
                 <tr>
                   <th>#</th>
                   <th>book name</th>
                   <th>author</th>
                   <th>type</th>
                   <th>price</th>
                 </tr>
                 <?php generateTable(); ?>
               </table>
             <?php else: ?>
               <h4>borrowed books</h4>
               <table>
                 <tr>
                   <th>#</th>
                   <th>book name</th>
                   <th>author</th>
                 </tr>
                 <?php getBooksToBorrow(); ?>
               </table>
             <?php endif; ?>
             <br><br>
           </div>
         </div>
       <?php else: ?>

         <p class="error">no items in cart</p>

       <?php endif; ?>
       <div class="row">
         <div class="step col-sm-10">

         <?php confirmBorrow(); ?>
         <form class="ui-form" action="" method="post">
       <?php if (isset($_GET['buy_books'])): ?>

             <h4>payment</h4>
             <b>Enter credit number</b>
             <br>

             <br><br>
               <?php confirmPurchase(); ?>
               <input type="text" name="credit-card" placeholder="credit card"><br>
               <input type="text" name="amount" placeholder="amount" value="<?php echo $_SESSION['total']; ?>"><br><br>
               <p>add deliverly 400ksh <input type="checkbox" name="deliverly" <?php if (isset($_SESSION['delivery'])) {echo "checked";} ?>></p>


       <?php endif; ?>
     </div>
     </div>


       <!-- payment options -->



       <?php if (isset($_GET['buy_books'])): ?>
         <br><br><p style="text-align:center;"><input type="submit" name="confirm_purchase" class="button green" value="confirm purchase"></p>
       <?php else: ?>
         <br><br><p style="text-align:center!important;color:green!important;"><input type="submit" name="confirm_borrow" class="button green" value="confirm borrow"></p>
       <?php endif; ?>
     </form>
     </div>

    </section>

  </body>
</html>
