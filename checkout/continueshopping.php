<?php require_once '../engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>checkout</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  </head>
  <body style="background-color:#b5dcfc;">

    <div class="top-menu">
      <ul>
        <li><a href='../'>home</a></li>
        <?php echo respectiveMenu('../'); ?>
        <li><a href='../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>


    <!-- continue shopping section -->
    <div class='cart-empty'>

      <h1>Your request has been received, continue browsing?</h1><br><br>

      <a href='../' class="button">home</a>

    </div>


  </body>
</html>
