<?php require_once '../engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>cart</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <style media="screen">
      .book_purchase{
        width:19%!important;
      }
    </style>
  </head>
  <body style="background-color:#b5dcfc;">


    <div class="top-menu">
      <ul>
        <li><a href='../'>home</a></li>
        <?php echo respectiveMenu('../'); ?>
        <li><a href='../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <!-- Book cart holding section -->
    <section>

      <div class="container-fluid book_container">
        <div class="row">
          <div class="col-sm-9">

            <?php if (isset($_SESSION['cartvalues'])):?>


              <?php if (count($_SESSION['cartvalues']) > 0): ?>

                <?php getItemsInCart(); ?>

              <?php else: ?>

                <div class='cart-empty'>

                  <h1>Your cart is empty, continue browsing?</h1><br><br>

                  <a href='../' class="button">home</a>

                </div>

              <?php endif; ?>

            <?php else: ?>

              <div class='cart-empty'>

                <h1>Your cart is empty, continue browsing?</h1><br><br>

                <a href='../' class="button">home</a>

              </div>

            <?php endif; ?>

          </div>
          <div class="col-sm-3 table_design">

            <?php if (isset($_SESSION['cartvalues'])):?>

              <table>

                <tr>

                  <th>#</th>

                  <th>book name</th>

                  <th>author</th>

                  <th>type</th>

                  <th>price</th>

                </tr>

                <?php generateTable(); ?>

              </table>

              <br><br>

              <?php if (count($_SESSION['cartvalues']) > 0): ?>

                <a href="../checkout/?buy_books" class="button green">buy books</a>

                <?php

                  if (isset($_COOKIE['user'])) {

                    if (getUserMembership($_COOKIE['user']) == 1) {

                      echo '<a href="../checkout/?borrow_books" class="button green">borrow books</a>';

                    }

                  }

                ?>

              <?php endif; ?>

            <?php endif; ?>

          </div>
        </div>
      </div>
    </section>

  </body>
</html>
