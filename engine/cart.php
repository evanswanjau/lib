<?php

/*
* CART FUNCTIONS
*/


require_once 'infused_cogs.php';





# function get value posted

function getValue(){

  if (isset($_GET['add_book_to_cart']) && $_GET['add_book_to_cart'] != '') {

    addToCart($_GET['add_book_to_cart'], 'book');


  }elseif (isset($_GET['add_ebook_to_cart']) && $_GET['add_ebook_to_cart'] != '') {

    addToCart($_GET['add_ebook_to_cart'], 'ebook');

  }


  if (isset($_SESSION['cartvalues'])){

    echo count($_SESSION['cartvalues']);

  }else {

    echo 0;

  }

}




# function to add value to array

function addToCart($id, $type){


  if (!isset($_SESSION['cartvalues'])) {

    $_SESSION['cartvalues'] = array();

    $_SESSION['cartvalues'][] = array('id' => $id, 'type' => $type);


  }else {

    $_SESSION['cartvalues'][] = array('id' => $id, 'type' => $type);

  }

}

############################################################################# checking out functions

# function to get products in cart

function getItemsInCart(){

  $book_data = new CrudFunctionality;


  # loop out all items in cart array

  for ($i=0; $i < count($_SESSION['cartvalues']); $i++) {

    $type = $_SESSION['cartvalues'][$i]['type'].'s';

    $id = $_SESSION['cartvalues'][$i]['id'];


    if ($type == 'books') {

      $value = 'book_id';

    }else {

      $value = 'ebook_id';

    }


    # get array of books from db

    $book_values = $book_data->getData('*', $type, $value, $id);


    # loop out itr=ems from db array with remove option

    for ($x=0; $x < count($book_values); $x++) {

      $name = $book_values[$x]['name'];

      $author = $book_values[$x]['author'];

      $price = $book_values[$x]['price'];

      $image = $book_values[$x]['image'];


      echo "
      <div class='col-sm-1 book_purchase'>

        <img src='../admin/covers/$image'><br><br>

        <p class='cap'><b>Book Name: </b>$name</p>

        <p class='cap'><b>Author: </b>$author</p>

        <p class='cap'><b>Price: </b>$price Ksh</p><br>

        <a href='?remove_from_cart=$id' class='button red'>remove from cart</a><br><br>

      </div>";

    }

  }

}








# call function when remove from cart is set

if (isset($_GET['remove_from_cart'])) {

  removeFromCart($_GET['remove_from_cart']);

}



# function to remove item from cartlist

function removeFromCart($id){

  $values = $_SESSION['cartvalues'];

  $countincart = count($values);

  $i = -1;

  foreach ($values as $value){

    $i++;

    if($values[$i]['id'] == $id){

      array_splice($values,$i,1);

      $_SESSION['cartvalues'] = $values;

      break;

    }

    $_SESSION['cartvalues'] = $values;

  }

}





# function to generate table

function generateTable(){

  $book_data = new CrudFunctionality;

  $count = 0;

  $total = 0;

  $_SESSION['total'] = $total;


  for ($i=0; $i < count($_SESSION['cartvalues']); $i++) {

    $type = $_SESSION['cartvalues'][$i]['type'].'s';

    $id = $_SESSION['cartvalues'][$i]['id'];

    if ($type == 'books') {

      $value = 'book_id';

    }else {

      $value = 'ebook_id';

    }


    # get array of books from db

    $book_values = $book_data->getData('*', $type, $value, $id);


    # loop out itr=ems from db array with remove option

    for ($x=0; $x < count($book_values); $x++) {

      $count++;

      $name = $book_values[$x]['name'];

      $author = $book_values[$x]['author'];

      $price = $book_values[$x]['price'];

      $image = $book_values[$x]['image'];


      echo "
      <tr>

        <td>$count</td>

        <td>$name</td>

        <td>$author</td>

        <td>$type</td>

        <td>$price</td>

      </tr>";

      # get total cost

      $total += $price;

    }

  }

  $_SESSION['total'] = $total;

  echo "

    <tr style='border-top:2px dotted #333;border-bottom:2px dotted #333;'>

      <td>&nbsp;</td>

      <td>&nbsp;</td>

      <td>&nbsp;</td>

      <td><b>TOTAL</b></td>

      <td><b>".$_SESSION['total']."</b></td>

    </tr>

  ";

}





# function to generate table

function generateOrderTable($order, $email){

  $book_data = new CrudFunctionality;

  $count = 0;

  $total = 0;



  global $conn;

  $purchase_codes = array();

  $sql = "SELECT * FROM purchases WHERE email = '$email' AND code = '$order' ORDER BY purchase_id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $count++;

      $id = $row['book_id'];

      $type = $row['type'];

      $delivery = $row['delivery'];

      // getPrice($type, $id);
      $price = 0;

      $data = getPurchaseDetails($type, $id);

      $name = $data[0]['name'];

      $author = $data[0]['author'];

      $price = $data[0]['price'];

      if ($type == 'ebook') {

        $link = $data[0]['ebook'];

        $link = "<br><a href='../../admin/ebook-storage/$link' class='button' download>download</a>";

      }else {

        $link = "-";

      }




      echo "
      <tr>

        <td>$count</td>

        <td>$name</td>

        <td>$author</td>

        <td>$type</td>

        <td>$link</td>

        <td>$price</td>

      </tr>";

      $total += $price;

    }

    if ($delivery == true ) {

      $total = $total + 400;

      echo "<tr>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td><b>delivery</b></td>

        <td><b>400</b></td>

      </tr>";

    }


    echo "
      <tr style='border-top:2px dotted #333;border-bottom:2px dotted #333;'>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td>&nbsp;</td>

        <td><b>TOTAL</b></td>

        <td><b>$total</b></td>

      </tr>

    ";


  }

}





# get single item price

function getPurchaseDetails($type, $id){

  $type = $type.'s';

  if ($type == 'books') {

    $value = 'book_id';

  }else {

    $value = 'ebook_id';

  }

  $product = new CrudFunctionality;

  $author = $product->getData('*', $type, $value, $id);

  return $author;

}




# get books to borrow

function getBooksToBorrow(){

  $book_data = new CrudFunctionality;

  $count = 0;

  $total = 0;

  $_SESSION['total'] = $total;


  for ($i=0; $i < count($_SESSION['cartvalues']); $i++) {

    $type = $_SESSION['cartvalues'][$i]['type'].'s';

    $id = $_SESSION['cartvalues'][$i]['id'];

    if ($type == 'books') {

      $value = 'book_id';

    }else {

      $value = 'ebook_id';

    }


    # get array of books from db

    $book_values = $book_data->getData('*', $type, $value, $id);


    # loop out itr=ems from db array with remove option

    for ($x=0; $x < count($book_values); $x++) {

      $count++;

      $name = $book_values[$x]['name'];

      $author = $book_values[$x]['author'];

      $price = $book_values[$x]['price'];

      $image = $book_values[$x]['image'];


      echo "
      <tr>

        <td>$count</td>

        <td>$name</td>

        <td>$author</td>

      </tr>";

      # get total cost

      $total += $price;

    }

  }


}



 ?>
