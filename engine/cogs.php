<?php

require_once 'infused_cogs.php'; # file link



# function to clean data

function clean_data($data) {

  $data = trim($data);

  $data = stripslashes($data);

  $data = htmlspecialchars($data);

  $data = str_replace("'","`", $data);


  return $data;

}



# funtion to shuffle string

function stringShuffle($divide = 4){

  $string = 'abcdefghijklmnopqrst0123456789';

  $string = str_shuffle($string);

  $code = substr($string, 0, strlen($string)/$divide);


  return $code;

}



# function to add s

function addS($time){
  if ($time != 1) {
    $s = 's';
  }else {
    $s = '';
  }

  return $s;
}



# function to get time difference

function timeSpan($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime($date) + 2592000;

  $seconds = $seconds - strtotime($current_date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." second".addS($secs);
    else if($seconds < 60*60 )
        $time = $mins." min".addS($mins);
    else if($seconds < 24*60*60)
        $time = $hours." hour".addS($hours);
    else if($seconds < 30*24*60*60)
        $time = $day." day".addS($day);
    else
        $time = $months." month".addS($months);

    return $time;
}

function yearSpan($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime($date) + 33696000;

  $seconds = $seconds - strtotime($current_date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." second".addS($secs);
    else if($seconds < 60*60 )
        $time = $mins." min".addS($mins);
    else if($seconds < 24*60*60)
        $time = $hours." hour".addS($hours);
    else if($seconds < 30*24*60*60)
        $time = $day." day".addS($day);
    else
        $time = $months." month".addS($months);

    return $time;
}


function timeDiff($date){

  # gets current date
  date_default_timezone_set('Africa/Nairobi');

  $current_date = date('Y-m-d H:i:s');



  # gets difference bettween date posted and current date

  $seconds  = strtotime(date($current_date)) - strtotime($date);

  $months = floor($seconds / (3600*24*30));

  $day = floor($seconds / (3600*24));

  $hours = floor($seconds / 3600);

  $mins = floor(($seconds - ($hours*3600)) / 60);

  $secs = floor($seconds % 60);


  if($seconds < 60)

      $time = $secs." second".addS($secs);

  else if($seconds < 60*60 )

      $time = $mins." min".addS($mins);

  else if($seconds < 24*60*60)

      $time = $hours." hour".addS($hours);

  else if($seconds < 30*24*60*60)

      $time = $day." day".addS($day);

  else

      $time = $months." month".addS($months);



  return $time;

}



function daySpan($date){

  date_default_timezone_set('Africa/Nairobi');
  $current_date = date('Y-m-d H:i:s');

  $seconds  = strtotime($date) + 2592000;

  $seconds = $seconds + strtotime($current_date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." second".addS($secs);
    else if($seconds < 60*60 )
        $time = $mins." min".addS($mins);
    else if($seconds < 24*60*60)
        $time = $hours." hour".addS($hours);
    else if($seconds < 30*24*60*60)
        $time = $day." day".addS($day);
    else
        $time = $months." month".addS($months);

    return $time;
}









/**
 * CRUD CLASS
 */

class CrudFunctionality{



  # method to add items to db
  public function addItems($assoc, $table){

    global $conn;

    $return_value = null; # default return value

    $columns = array();

    $values = array();



    foreach ($assoc as $key => $value) {

      $columns[] = $key;

      $values[] = "'".$value."'";

    }




    # convert columns and data into sql format
    $column_data = implode($columns, ', ');

    $value_data = implode($values, ', ');



    #Inserting the user's data into our database
    $sql = "INSERT INTO $table ($column_data) VALUES ($value_data)";

    if ($conn->query($sql) === TRUE) {

      $return_value = true;

    }else {

      $return_value = "Error: " . $sql . "<br>" . $conn->error;

    }

    return $return_value;

  }




  # method to get items from db
  public function getData($item, $table, $field=0, $value=0, $order=0, $design=0){

    global $conn;

    $assoc = array();

    $return_value = null; # default return value



    # get items from db

    $sql = "SELECT $item FROM $table WHERE $field = '$value' ORDER BY '$order' '$design'";

    $result = $conn->query($sql);


    if ($result->num_rows > 0) {

      while($row = $result->fetch_assoc()) {

        $assoc[] = $row;

      }

      $return_value = $assoc;

    }else {

      $return_value = false;

    }



    return $return_value;

  }







  # method to update items in db

  public function updateData($table, $column, $value, $field, $clause){


    global $conn;

    $assoc = array();

    $return_value = false; # default return value

    # update item in db

    $sql = "UPDATE $table SET $column = '$value' WHERE $field = '$clause'";

    $result = $conn->query($sql);


    if ($conn->query($sql) === TRUE) {

      $return_value = true;

    }else {

      $return_value = "Error: " . $sql . "<br>" . $conn->error;

    }

    return $return_value;

  }





  # method to deelte item from db

  public function deleteItem($table, $field, $clause){

    global $conn;

    $return_value = null;

    $sql = "DELETE FROM $table WHERE $field = '$clause'";


    if ($conn->query($sql) === TRUE) {

      $return_value = 'true';

    }else {

      $return_value =  "Error: " . $sql . "<br>" . $conn->error;

    }

  }



}














/*
-------------------------------------------------------------------------------
USER FUNCTIONALITY
-------------------------------------------------------------------------------
*/


# register user function
function signUp(){

  global $conn;


  $errors = array();

  $user_data = array();

  $card_details = array();

  $user = new CrudFunctionality;



  # after button is clicked

  if (isset($_POST['signup'])) {



    # full name

    if($_POST['full_name'] != ''){

      $user_data['full_name'] = strtolower(clean_data($_POST['full_name']));

    }else {

      $errors[] = "<p class='error'>full name is required</p>";

    }





    # phone number

    if($_POST['phone'] != ''){

      $user_data['phone'] = strtolower(clean_data($_POST['phone']));

    }else {

      $errors[] = "<p class='error'>phone number is required</p>";

    }





    # location

    if($_POST['location'] != ''){

      $user_data['location'] = strtolower(clean_data($_POST['location']));

    }else {

      $errors[] = "<p class='error'>location is required</p>";

    }



    # email

    if($_POST['email'] != ''){

      # check if that user exists
      $return_value = $user->getData('email', 'users', 'email', $_POST['email']);


      if ($return_value != null) {

        $errors[] = "<p class='error'>that account already exists</p>";

        $user_data['email'] = null;

      }else {

        $user_data['email'] = clean_data($_POST['email']);

        $email = $user_data['email'];

      }

    }else {

      $errors['email'] = "<p class='error'>email is required</p>";

    }



    #password

    if($_POST['password'] != ''){

      $password = clean_data($_POST['password']);

      if (strlen($password) < 6) {

        $errors[] = "<p class='error'>password cannot be less than 6 characters</p>";

      }else {


        # confirm password

        if($_POST['password2'] != ''){

          $password2 = clean_data($_POST['password2']);

        }else {

          $errors[] = "<p class='error'>please confirm your password</p>";

        }


        # secure password

        if ($password == $password2) {

          $user_data['password'] = md5($password);

        }else {

          $errors[] = "<p class='error'>passwords are not similar</p>";

        }

      }

    }else {

      $errors[] = "<p class='error'>password is required</p>";

    }





    # when no errors exist

    if($errors == []){

        $return_value = $user->addItems($user_data, 'users');


        if ($return_value == TRUE) {

          setcookie('user', $email, time() + 43200, "/");

          header('Location: ../profile');

        }else {

          echo $return_value;

        }

      }else {

        foreach ($errors as $error) {

          echo $error;

        }

      }

    }

  }








# function to login the user
function loginUser(){

  global $conn;



  $errors = array();

  $user_data = array();

  $user = new CrudFunctionality;




  if (isset($_POST['login'])) {



      #username

      if($_POST['email'] != ''){

        $user_data['email'] = clean_data($_POST['email']);

      }else {

        $errors[] = "<p class='error'>email is required</p>";

      }



      #password

      if($_POST['password'] != ''){

        $user_data['password'] = clean_data($_POST['password']);

      }else {

        $errors[] = "<p class='error'>password is required</p>";

      }


      if($errors == []){


        # confirm user login details

        $return_value = $user->getData('email, password', 'users', 'email', $user_data['email']);

        if ($return_value != false) {

          $email = $return_value[0]['email'];

          $dbpassword = $return_value[0]['password'];


          # confirm password

          if (md5($user_data['password']) == $dbpassword) {

            setcookie('user', $email, time() + 43200, "/");

            if (isset($_SESSION['cartvalues'])) {

                if (count($_SESSION['cartvalues']) > 0) {

                  header('Location: ../cart');

                }else {

                  header('Location: ../profile');

                }

            }else {

              header('Location: ../profile');

            }



          }else {

           echo '<p class="error">invalid login details</p>';

         }

       }else {

          # confirm admin login details

          $return_admin_value = $user->getData('username, password', 'admin', 'username', $user_data['email']);

          if ($return_admin_value != false) {

            $email = $return_admin_value[0]['username'];

            $dbpassword = $return_admin_value[0]['password'];



            # confirm password

            if (md5($user_data['password']) == $dbpassword) {

              setcookie('admin', $email, time() + 43200, "/");

              header('Location: ../admin');


            } else {

              echo '<p class="error">invalid login details</p>';

            }

          }else {

            echo '<p class="error">that email does not exist</p>';

          }

        }

      }else{

        foreach ($errors as $error) {

          echo $error;

        }

      }

    }

}





# account security

function accountSecurity($link='', $account){

  if (!isset($_COOKIE[$account])) {

    header('location: '.$link.'login');

  }

}




# logout functionality
function logOut($link=''){

  if (isset($_GET['logout'])) {

    setcookie('admin', "", time() - 43200, "/");

    setcookie('user', "", time() - 43200, "/");

    header('location: '.$link.'');

  }

}










/*
-------------------------------------------------------------------------------
BOOK FUNCTIONALITY
-------------------------------------------------------------------------------
*/


# add new book
function addNewBook(){


  global $conn;

  $errors = array();

  $book_data = array();

  $book = new CrudFunctionality;




  if (isset($_POST['add-book'])) {



    # book name

    if($_POST['name'] != ''){

      $book_data['name'] = clean_data($_POST['name']);

    }else {

      $errors[] = "<p class='error'>name is required</p>";

    }




    # author

    if($_POST['author'] != ''){

      $book_data['author'] = clean_data($_POST['author']);

    }else {

      $errors[] = "<p class='error'>author is required</p>";

    }




    # price

    if($_POST['price'] != ''){

      $book_data['price'] = clean_data($_POST['price']);

    }else {

      $errors[] = "<p class='error'>price is required</p>";

    }




    # count

    if($_POST['count'] != ''){

      $book_data['count'] = clean_data($_POST['count']);

    }else {

      $errors[] = "<p class='error'>count is required</p>";

    }



    # image upload

    if(isset($_FILES['image'])){



      #Verify Image upload data

      $image_name = $_FILES['image']['name'];

      $image_size =$_FILES['image']['size'];

      $image_tmp =$_FILES['image']['tmp_name'];

      $image_type=$_FILES['image']['type'];

      $image_ext=strtolower(end((explode('.',$_FILES['image']['name']))));



      $extensions= array("jpeg","jpg","png");



      if ($image_name == '') {

        $book_data['image'] = 'default.png';

      }else {

        if(in_array($image_ext,$extensions)=== false){

           $errors[] = "<p class='error'>please choose a JPEG or PNG file</p>";

        }else {

          if($image_size > 4194304){

             $errors[]='<p class="error">image is too large</p>';

          }

        }

        $book_data['image'] = stringShuffle($divide = 3, $string='abcdefghijklmnopqrst').'.'.$image_ext;

      }



    }


    # if no errors exist
    if (empty($errors)) {

      $return_value = $book->addItems($book_data, 'books');


      if ($return_value == TRUE) {

        move_uploaded_file($image_tmp,'../covers/'.$book_data['image'].'');

        echo "<p class='success'>book added successfully</p>";

        header('refresh:1; url=../books');

      }else {

        echo $return_value;

      }


    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}









# add new ebook
function addNewEBook(){

  global $conn;

  $errors = array();

  $ebook_data = array();

  $ebook = new CrudFunctionality;



  if (isset($_POST['add-ebook'])) {



    # book name

    if($_POST['name'] != ''){

      $ebook_data['name'] = clean_data($_POST['name']);

    }else {

      $errors[] = "<p class='error'>name is required</p>";

    }



    # author

    if($_POST['author'] != ''){

      $ebook_data['author'] = clean_data($_POST['author']);

    }else {

      $errors[] = "<p class='error'>author is required</p>";

    }



    # price

    if($_POST['price'] != ''){

      $ebook_data['price'] = clean_data($_POST['price']);

    }else {

      $errors[] = "<p class='error'>price is required</p>";

    }



    # image upload

    if(isset($_FILES['image'])){


      #Verify Image upload data

      $image_name = $_FILES['image']['name'];

      $image_size =$_FILES['image']['size'];

      $image_tmp =$_FILES['image']['tmp_name'];

      $image_type=$_FILES['image']['type'];

      $image_ext=strtolower(end((explode('.',$_FILES['image']['name']))));



      $extensions= array("jpeg","jpg","png");



      if ($image_name == '') {

        $book_data['image'] = 'default.png';

      }else {

        if(in_array($image_ext,$extensions)=== false){

           $errors[] = "<p class='error'>please choose a JPEG or PNG file</p>";

        }else {

          if($image_size > 4194304){

             $errors[]='<p class="error">image is too large</p>';

          }

        }
        $ebook_data['image'] = stringShuffle($divide = 3, $string='abcdefghijklmnopqrst').'.'.$image_ext;

      }

    }



    # ebook upload

    if(isset($_FILES['ebook'])){

      # verify ebook upload data

      $ebook_name = $_FILES['ebook']['name'];

      $ebook_size =$_FILES['ebook']['size'];

      $ebook_tmp =$_FILES['ebook']['tmp_name'];

      $ebook_type=$_FILES['ebook']['type'];

      $ebook_ext=strtolower(end((explode('.',$_FILES['ebook']['name']))));



      $extensions= array("pdf","docx","doc");



      if ($ebook_name == '') {

        $errors[] = "<p class='error'>you have not uploaded a file</p>";

      }else {

        if(in_array($ebook_ext,$extensions)=== false){

           $errors[] = "<p class='error'>please choose a PDF or DOC, DOCX file</p>";

        }else {

          if($ebook_size > 4194304){

             $errors[]='<p class="error">ebook is too large</p>';

          }

        }

      }

      $ebook_data['ebook'] = stringShuffle($divide = 3, $string='abcdefghijklmnopqrst').'.'.$ebook_ext;

      $ebook_data['size'] = $ebook_size;

    }



    # if no errors exist

    if (empty($errors)) {


      $return_value = $ebook->addItems($ebook_data, 'ebooks');

      if ($return_value == TRUE) {

        move_uploaded_file($image_tmp,'../covers/'.$ebook_data['image']);

        move_uploaded_file($ebook_tmp,'../ebook-storage/'.$ebook_data['ebook']);

        echo "<p class='success'>ebook added successfully</p>";

        header('refresh:1; url=../ebooks');

      }else {

        echo $return_value;

      }

    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}








# edit book

function editBook($id){

  global $conn;

  $errors = array();

  $book_data = array();

  $book = new CrudFunctionality;

  $success = false;



  # on button click

  if (isset($_POST['edit-book'])) {


    # book name

    if($_POST['name'] != ''){

      $name = clean_data($_POST['name']);

      $return_value = $book->updateData('books', 'name', $name, 'book_id', $id);


      if ($return_value !== false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>name is required</p>";

    }





    # author

    if($_POST['author'] != ''){

      $author = clean_data($_POST['author']);

      $return_value = $book->updateData('books', 'author', $author, 'book_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>author is required</p>";

    }





    # price

    if($_POST['price'] != ''){

      $price = clean_data($_POST['price']);

      $return_value = $book->updateData('books', 'price', $price, 'book_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>price is required</p>";

    }





    # count

    if($_POST['count'] != ''){

      $count = clean_data($_POST['count']);


      $return_value = $book->updateData('books', 'count', $count, 'book_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>count is required</p>";

    }





    # if no errors exist

    if (empty($errors)) {

      if ($success == true) {

        echo "<p class='success'>book edited successfully</p>"; # return successful message

        header('refresh:1; url=../books');

      }

    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}













# edit ebook

function editEBook($id){


  global $conn;

  $errors = array();

  $ebook_data = array();

  $ebook = new CrudFunctionality;

  $success = false;




  if (isset($_POST['edit-ebook'])) {

    # book name

    if($_POST['name'] != ''){

      $name = clean_data($_POST['name']);

      $return_value = $ebook->updateData('ebooks', 'name', $name, 'ebook_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>name is required</p>";

    }





    # author

    if($_POST['author'] != ''){

      $author = clean_data($_POST['author']);

      $return_value = $ebook->updateData('ebooks', 'author', $author, 'ebook_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>author is required</p>";

    }





    # price

    if($_POST['price'] != ''){

      $price = clean_data($_POST['price']);

      $return_value = $ebook->updateData('ebooks', 'price', $price, 'ebook_id', $id);


      if ($return_value != false) {

        $success = true;

      }else {

        echo $return_value;

      }

    }else {

      $errors[] = "<p class='error'>price is required</p>";

    }





    # if no errors exist

    if (empty($errors)) {

      if ($success == true) {

        echo "<p class='success'>ebook edited successfully</p>"; # return successful message

        header('refresh:1; url=../ebooks');

      }

    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}




# get books

function getBooks(){

  global $conn;

  $sql = "SELECT * FROM books ORDER BY book_id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $id = $row['book_id'];

      $name = $row['name'];

      $author = $row['author'];

      $price = $row['price'];

      $count = $row['count'];

      $image = $row['image'];

      if ($count < 1) {

        $style = "style='background-color:#999;'";

        $value = null;

      }else {

        $style = null;

        $value = "<a href='../assign-book/?assign_book_id=$id' class='button'>assign book</a>";

      }



      echo "
      <div class='col-sm-6 present-book' $style>

        <div class='row'>

          <div class='col-sm-3'>

            <img src='../covers/$image'>

          </div>

          <div class='col-sm-9 text'>

            <p><b>Name: </b> $name</p>

            <p><b>Author: </b> $author</p>

            <p><b>Price: </b>$price Ksh</p>

            <p><b>Books Remaining: </b> $count</p>

            <br>

            $value

            <a href='../add-book/?edit_book_id=$id' class='button'>edit</a>

            <a href='?delete_book_id=$id' class='button del'>delete</a>

          </div>

        </div>

      </div>";
    }

  }

}



# get ebooks

function getEBooks(){

  global $conn;

  $sql = "SELECT * FROM ebooks ORDER BY ebook_id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $id = $row['ebook_id'];

      $name = $row['name'];

      $author = $row['author'];

      $price = $row['price'];

      $image = $row['image'];


      echo "

      <div class='col-sm-6 present-book'>

        <div class='row'>

          <div class='col-sm-3'>

            <img src='../covers/$image'>

          </div>

            <div class='col-sm-9 text'>

            <p><b>Name: </b> $name</p>

            <p><b>Author: </b> $author</p>

            <p><b>Price: </b>$price Ksh</p>

            <br>

            <a href='../add-ebook/?edit_ebook_id=$id' class='button'>edit</a>

            <a href='?delete_ebook_id=$id' class='button del'>delete</a>

          </div>

        </div>

      </div>";
    }

  }

}




# delete books

function deleteBook(){

  $book = new CrudFunctionality;

  if (isset($_GET['delete_book_id'])) {

    $delete = $book->deleteItem('books', 'book_id', $_GET['delete_book_id']);

  }

}


# delete ebook

function deleteEBook(){

  $book = new CrudFunctionality;

  if (isset($_GET['delete_ebook_id'])) {

    $delete = $book->deleteItem('ebooks', 'ebook_id', $_GET['delete_ebook_id']);

  }

}







# get user data
function getUserData($value, $id){

  global $conn;

  $assign = new CrudFunctionality;


  # borrow_count

  $borrow_info = $assign->getData('borrows', 'library_cards', 'card_no', $value);


  if ($borrow_info[0]['borrows'] == 2) {

    echo "<p class='error'>account has already borrowed 2 books on that card</p>";

  }else {

    $sql = "SELECT * FROM library_cards WHERE card_no = '$value'";

    $result = $conn->query($sql);


    if($result->num_rows > 0){

      while($row = $result->fetch_assoc()) {

        $card_no = strtoupper($row['card_no']);

        $email = $row['email'];

        $assign_info = $assign->getData('*', 'users', 'email', $email);

      }

      echo "
      <div class='assign'>

        <h3>Issue book to this user?</h3><br>

        <p><b>Card ID: </b> $card_no</p>

        <p class='cap'><b>Full Name: </b>".$assign_info[0]['full_name']."</p>

        <p><b>Email: </b>$email</p>

        <br>

        <a href='?assign_book_id=$id&assign_lib_id=$card_no&email=$email' class='button'>Issue</a>

      </div>";

    }else {

      echo "<p class='error'>that library card does not exist</p>";

    }

  }

}








# assign functionality

function assignBook($book_id, $lib_id, $email){


  $assign = new CrudFunctionality;

  $borrow_data = array();


  # assign book data array

  $borrow_data['book_id'] = $book_id;

  $borrow_data['card_no'] = $lib_id;

  $borrow_data['email'] = $email;

  date_default_timezone_set('Africa/Nairobi');

  $current_date = date('Y-m-d H:i:s');

  $borrow_data['due_date'] = date('Y-m-d H:i:s', strtotime($current_date. ' + 30 days'));

  $borrow_data['status'] = 1;




  # update borrow history

  $return_value = $assign->addItems($borrow_data, 'borrowed_books');

  if ($return_value == true){


    # update book count

    $book_count_array = $assign->getData('count', 'books', 'book_id', $book_id);

    $count = $book_count_array[0]['count'];

    $new_count = $count - 1;

    $assign->updateData('books', 'count', $new_count, 'book_id', $book_id);


    # update borrow card count

    $borrows_array = $assign->getData('borrows', 'library_cards', 'card_no', $lib_id);

    $borrow_count = $borrows_array[0]['borrows'];

    $new_borrow_count = $borrow_count + 1;

    $assign->updateData('library_cards', 'borrows', $new_borrow_count, 'card_no', $lib_id);


    # return success message

    echo "<p class='success'>book issued successfully</p>";

    header('refresh:1; url=../');

  }else {

    echo $return_value;

  }

}







# get borrowed books

function getBorrowedBooks(){

  $count = 0;

  $borrowed_books = new CrudFunctionality;

  $return_value = $borrowed_books->getData('*', 'borrowed_books');

  if ($return_value != false) {

    echo "

    <div class='col-sm-12 formal parent'>

      <ul>

        <span><b>#</b></span>

        <li><b>Book Name</b></li>

        <li><b>Borrowed by</b></li>

        <li><b>Days Remaining</b></li>

        <li><b>amount due</b></li>

        <li><b>status</b></li>

      </ul>

    </div>";

    # print out data

    for ($i=0; $i < count($return_value); $i++) {

      $count++;

      $id = $return_value[$i]['id'];

      $book_id = $return_value[$i]['book_id'];

      $lib_id = $return_value[$i]['card_no'];

      $timestamp = $return_value[$i]['timestamp'];

      $due_date = $return_value[$i]['due_date'];

      $status = $return_value[$i]['status'];


      # check overdue time

      if (timeSpan($timestamp) <= 0) {

        $value = 'overdue by ' . timeDiff($due_date);

        $amount_due = (timeDiff($due_date) * 5) . 'ksh';

      }else {

        $value = timeSpan($timestamp);

        $amount_due = 0;

      }


      if ($status == 1) {

        $status_value = 'approved';

      }else {

        $status_value = "<a href='?approve_book=$id' class='button'>approve</a>";

      }


      echo "
      <div class='col-sm-12 parent'>

        <ul>

          <span>$count</span>

          <li class='cap'>".getBookName($book_id)."</li>

          <li class='cap'>".getUserName($lib_id)."</li>

          <li>$value</li>

          <li>$amount_due</li>

          <li>$status_value</li>

          <a href='?return_borrow_id=$id&return_book_id=$book_id&card_no=$lib_id' class='right'>return&nbsp;book</a>

        </ul>

      </div>";

    }

  }else {

    echo "<p class='error' style='text-align:center'>No books have been borrowed</p>";

  }

}



# approve book borrow

function approveBorrow(){

  $approve = new CrudFunctionality;

  if (isset($_GET['approve_book'])) {

    $id = $_GET['approve_book'];

    $return_value = $approve->updateData('borrowed_books', 'status', 1, 'id', $id);

    if ($return_value != null) {

      header('location: ../borrowed-books/');

    }else {

      echo $return_value;

    }

  }

}






# get user by lib ID

function getUserName($id){

  $user_data = new CrudFunctionality;

  $return_value = $user_data->getData('email', 'borrowed_books', $field='card_no', $id);

  $email = $return_value[0]['email'];

  $personal_info = $user_data->getData('*', 'users', 'email', $email);

  return $personal_info[0]['full_name'];

}




# get book by book ID

function getBookName($id){

  $book_data = new CrudFunctionality;

  $return_value = $book_data->getData('name', 'books', $field='book_id', $id);


  return $return_value[0]['name'];

}



# return book

function returnBook($borrow_id, $book_id, $card_no){


  $book_data = new CrudFunctionality;


  # update book count

  $book_count_array = $book_data->getData('count', 'books', 'book_id', $book_id);

  $count = $book_count_array[0]['count'];

  $new_count = $count + 1;

  $book_data->updateData('books', 'count', $new_count, 'book_id', $book_id);


  # update card number

  $borrow_count_array = $book_data->getData('borrows', 'library_cards', 'card_no', $card_no);

  $borrow_count = $borrow_count_array[0]['borrows'];

  $new_borrow_count = $borrow_count - 1;

  $book_data->updateData('library_cards', 'borrows', $new_borrow_count, 'card_no', $card_no);


  # remove from borrowing list

  $book_data->deleteItem('borrowed_books', 'id', $borrow_id);


}








/*
-------------------------------------------------------------------------------
PURCHASE FUNCTIONS
-------------------------------------------------------------------------------
*/

function getPurchaseBooks(){

  $purchase_books = new CrudFunctionality;

  $return_book_value = $purchase_books->getData('*', 'books');


  # echo our books

  for ($i=0; $i < count($return_book_value); $i++) {

    $id= $return_book_value[$i]['book_id'];

    $name = $return_book_value[$i]['name'];

    $author = $return_book_value[$i]['author'];

    $price = $return_book_value[$i]['price'];

    $count = $return_book_value[$i]['count'];

    $image = $return_book_value[$i]['image'];


    # out of stock button output

    if ($count < 1) {

      $value = "<a href='#' disabled class='button'>out of stock</a><br><br>";

      $style = "style='background-color:#999;'";

    }else {
      $value = "<a href='?add_book_to_cart=$id' class='button'> add to cart</a><br><br>";

      $style = null;

    }



    echo "
    <div class='col-sm-1 book_purchase' $style>

      <img src='admin/covers/$image'><br><br>

      <p class='cap'><b>Book Name: </b>$name</p>

      <p class='cap'><b>Author: </b>$author</p>

      <p class='cap'><b>Price: </b>$price Ksh</p>

      <p class='cap'><b>Books Remaining: </b>$count</p><br>

      $value

    </div>";

  }

}




# get Ebooks fro user display

function getPurchaseEBooks(){

  $purchase_books = new CrudFunctionality;

  $return_ebook_value = $purchase_books->getData('*', 'ebooks');



  # echo our books

  for ($i=0; $i < count($return_ebook_value); $i++) {

    $id= $return_ebook_value[$i]['ebook_id'];

    $name = $return_ebook_value[$i]['name'];

    $author = $return_ebook_value[$i]['author'];

    $price = $return_ebook_value[$i]['price'];

    $image = $return_ebook_value[$i]['image'];

    $size = $return_ebook_value[$i]['size'];

    # convert to mb

    $size = intVal($size / 1000000);



    echo "
    <div class='col-sm-1 book_purchase'>

      <img src='admin/covers/$image'><br><br>

      <p class='cap'><b>Book Name: </b> $name</p>

      <p class='cap'><b>Author: </b> $author</p>

      <p class='cap'><b>Price: </b> $price Ksh</p>

      <p class='cap'><b>Size: </b> $size MB</p><br>

      <a href='?add_ebook_to_cart=$id' class='button'> add to cart</a><br><br>

    </div>";

  }

}





# confirm purchase

function confirmPurchase(){

  $payment = new CrudFunctionality;

  $payment_info = array();

  $errors = array();


  # on button click

  if (isset($_POST['confirm_purchase'])) {


    if (isset($_POST['deliverly'])) {

      $deliverly = 1;

      $_SESSION['total'] = $_SESSION['total'] + 400;

      $_SESSION['deliverly'] = true;

    }else {

      $deliverly = 0;

    }




    # camount

    if ($_POST['amount'] != '') {

      if ($_POST['amount'] == $_SESSION['total']) {

        $payment_info['amount'] = $_POST['amount'];

      }elseif ($_POST['amount'] > $_SESSION['total']) {

        $errors[] = "<p class='error'>amount is greater than required</p>";

      }elseif ($_POST['amount'] < $_SESSION['total']) {

        $errors[] = "<p class='error'>amount is lower than required</p>";

      }

    }else {

      $errors[] = "<p class='error'>amount cannot be emepty</p>";

    }






    # credit card

    if ($_POST['credit-card'] != '') {

      if (strlen($_POST['credit-card']) < 12) {

        $errors[] = "<p class='error'>card number cannot be less than 12 characters</p>";

      }

    }else {

      $errors[] = "<p class='error'>card number cannot be emepty</p>";

    }




    # purchase code

    $payment_info['purchase_code'] = stringShuffle($divide=4);





    if (empty($errors)) {

      for ($i=0; $i < count($_SESSION['cartvalues']); $i++) {

        # items for orders table

        $items_array = array();

        $items_array['book_id'] = $_SESSION['cartvalues'][$i]['id'];

        $items_array['type'] = $_SESSION['cartvalues'][$i]['type'];

        $items_array['code'] = $payment_info['purchase_code'];

        $items_array['email'] = $_COOKIE['user'];

        $items_array['delivery'] = 1;



        # items still in cart - will be used to update db

        $type = $_SESSION['cartvalues'][$i]['type'].'s';

        $id = $_SESSION['cartvalues'][$i]['id'];



        if ($type == 'books') {

          $value = 'book_id';

        }else {

          $value = 'ebook_id';

        }

        $return_value = $payment->addItems($items_array, 'purchases');

        if ($return_value != null) {

          # update book count

          if ($type == 'books') {

            $book_count_array = $payment->getData('count', $type, $value, $id);

            $count = $book_count_array[0]['count'];

            $new_count = $count + 1;

            $payment->updateData($type, 'count', $new_count, $value, $id);

          }

        }else {
          echo "string";
        }

      }

      # unset sessions

      $_SESSION['cartvalues'] = [];

      $_SESSION['delivery'] = [];

      header('location: continueshopping.php');


    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}






# confirm purchase

function confirmBorrow(){

  $assign = new CrudFunctionality;

  $borrow_data = array();

  $success = null;

  $errors = array();




  # on button click

  if (isset($_POST['confirm_borrow'])) {


    if (getCardNumber($_COOKIE['user']) === 0) {

      $errors[] = "<p class='error'>You have exhausted your library cards</p>";

    }



    for ($i=0; $i < count($_SESSION['cartvalues']); $i++) {


      $items_array = array();

      $items_array['book_id'] = $_SESSION['cartvalues'][$i]['id'];

      $items_array['email'] = $_COOKIE['user'];

      $items_array['card_no'] = getCardNumber($_COOKIE['user']);



      date_default_timezone_set('Africa/Nairobi');

      $current_date = date('Y-m-d H:i:s');

      $items_array['due_date'] = date('Y-m-d H:i:s', strtotime($current_date. ' + 30 days'));

      $items_array['status'] = '0';




      if (empty($errors)) {


        if ($_SESSION['cartvalues'][$i]['type'] == 'book') {

          $return_value = $assign->addItems($items_array, 'borrowed_books');

          if ($return_value == true){


            # update book count

            $book_count_array = $assign->getData('count', 'books', 'book_id', $items_array['book_id']);

            $count = $book_count_array[0]['count'];

            $new_count = $count - 1;

            $assign->updateData('books', 'count', $new_count, 'book_id', $items_array['book_id']);


            # update borrow card count

            $borrows_array = $assign->getData('borrows', 'library_cards', 'card_no', $items_array['card_no']);

            $borrow_count = $borrows_array[0]['borrows'];

            $new_borrow_count = $borrow_count + 1;


            $assign->updateData('library_cards', 'borrows', $new_borrow_count, 'card_no', $items_array['card_no']);

            $new_borrow_count = 0;


            $success = true;

          }else {

            echo $return_value;

          }

        }

      }

    }

    if ($success != null) {
      # return success message

      $_SESSION['cartvalues'] = [];

      echo "<p class='success'>book issued successfully</p>";

      header('refresh:2; url=continueshopping.php');
    }


    if (!empty($errors)) {

      foreach ($errors as $error) {

        echo $error;

      }

      header('refresh:2; url=../profile/membership');

    }


  }

}




# get card number

function getCardNumber($user){

  global $conn;

  $sql = "SELECT * FROM library_cards WHERE email = '$user' AND borrows < 2 ORDER BY id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $card_no = $row['card_no'];

    }

    return $card_no;

  }else {

    return 0;

  }




}






# get borrowed books

function getUserBorrowedBooks($user){

  $count = 0;

  $borrowed_books = new CrudFunctionality;

  $return_value = $borrowed_books->getData('*', 'borrowed_books', 'email', $user);


  if ($return_value != false) {

    echo "

    <div class='col-sm-12 formal parent'>

      <ul>

        <span><b>#</b></span>

        <li><b>Book Name</b></li>

        <li><b>Borrowed by</b></li>

        <li><b>Days Remaining</b></li>

        <li><b>Amount Due</b></li>

        <li><b>Borrow Status</b></li>

      </ul>

    </div>";

    # print out data

    for ($i=0; $i < count($return_value); $i++) {

      $count++;

      $id = $return_value[$i]['id'];

      $book_id = $return_value[$i]['book_id'];

      $lib_id = $return_value[$i]['card_no'];

      $timestamp = $return_value[$i]['timestamp'];

      $due_date = $return_value[$i]['due_date'];

      $status = $return_value[$i]['status'];


      # check overdue time

      if (timeSpan($timestamp) <= 0) {

        $value = 'overdue by ' . timeDiff($due_date);

        $amount_due = (timeDiff($due_date) * 5) . 'ksh';

      }else {

        $value = timeSpan($timestamp);

        $amount_due = 0;

      }


      if ($status == 1) {

        $status_value = 'approved';

      }else {

        $status_value = "waiting approval";

      }


      echo "
      <div class='col-sm-12 parent'>

        <ul>

          <span>$count</span>

          <li class='cap'>".getBookName($book_id)."</li>

          <li class='cap'>".getUserName($lib_id)."</li>

          <li>$value</li>

          <li>$amount_due</li>

          <li>$status_value</li>

        </ul>

      </div>";

    }

  }else {

    echo "<p class='error' style='text-align:center'>No books have been borrowed</p>";

  }

}



function getUserOrders($email){

  $count = 0;

  global $conn;

  $purchase_codes = array();

  $sql = "SELECT * FROM purchases WHERE email = '$email' GROUP BY code ORDER BY purchase_id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    echo "

    <div class='col-sm-12 formal parent'>

      <ul>

        <span><b>#</b></span>

        <li><b>Purchase Code</b></li>

        <li><b>Purchase Date</b></li>

      </ul>

    </div>";

    while($row = $result->fetch_assoc()) {

      $count++;

      $code = $row['code'];

      $date = $row['date'];

      echo "

      <a href='purchases/?purchase_code=$code' style='color:#333;;'>

      <div class='col-sm-12 parent' style='cursor:pointer!important;'>

        <ul>

          <span>$count</span>

          <li class='cap'>".strtoupper($code)."</li>

          <li class='cap'>".$date."</li>

        </ul>

      </div>

      </a>";


    }

  }else {
    echo "<p class='error' style='text-align:center;'>you have not made any purchases</p>";
  }

}



# echo respective menu

function respectiveMenu($link='../'){

  if (isset($_COOKIE['user'])) {

    $value = "<li><a href='".$link."profile'>myprofile</a></li>";

  } else {

    $value = "<li><a href='".$link."login'>login</a></li><li><a href='".$link."signup'>signup</a></li>";

  }

  return $value;


}



# show admin side purchases

function adminPurchases(){

  global $conn;

  $order_codes = array();

  $sql = "SELECT code, email, date FROM purchases GROUP BY code ORDER BY purchase_id DESC";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $order_codes[] = $row['code'];

    }

  }


  for ($i=0; $i < count($order_codes); $i++) {

    $code = $order_codes[$i];

    $count = 0;

    $total = 0;



    $sql = "SELECT * FROM purchases WHERE code = '$code'";

    $result = $conn->query($sql);

    if($result->num_rows > 0){

      echo "
      <div class='purchase_view col-sm-11 table_design'>

      <h4>PURCHASE CODE - #".strtoupper($code)."</h4>
      <br>

      <table>
        <tr>
          <th>#</th>

          <th>book name</th>

          <th>author</th>

          <th>type</th>

          <th>price</th>

        </tr>
      ";

      while($row = $result->fetch_assoc()) {

        $count++;

        $id = $row['book_id'];

        $type = $row['type'];

        $date = $row['date'];

        $email = $row['email'];

        $delivery = $row['delivery'];

        $item_details = getPurchaseDetails($type, $id);

        $name = $item_details[0]['name'];

        $author = $item_details[0]['author'];

        $price = $item_details[0]['price'];

        echo "
        <tr>

          <td>$count</td>

          <td>$name</td>

          <td>$author</td>

          <td>$type</td>

          <td>$price Ksh</td>

        </tr>";

        $total += $price;

      }

      if ($delivery == true ) {

        $total = $total + 400;

        echo "<tr>

          <td>&nbsp;</td>

          <td>&nbsp;</td>

          <td>&nbsp;</td>

          <td><b>delivery</b></td>

          <td><b>400</b></td>

        </tr>";

      }

      echo "
        <tr style='border-top:2px dotted #333;border-bottom:2px dotted #333;'>

          <td>&nbsp;</td>

          <td>&nbsp;</td>

          <td>&nbsp;</td>

          <td><b>TOTAL</b></td>

          <td><b>$total</b></td>

        </tr>

      ";

      echo "</table><br>

      <h4>Purchased <b>".timeDiff($date)." ago by $email</b></h4>

      </div>";

    }

  }

}


# get admin notifications

function getAdminNotifications(){

  global $conn;

  $count = 0;



  # array holders

  $notifications_array = array();



  # get date 3 days ago
  date_default_timezone_set('Africa/Nairobi');

  $current_date = date('Y-m-d H:i:s');

  $date=date_create($current_date);

  date_sub($date,date_interval_create_from_date_string("3 days"));

  $three_days_ago = date_format($date,"Y-m-d H:i:s");







  # get members

  $sql = "SELECT * FROM users WHERE date >= '$three_days_ago'";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $notifications_array[] = array('type' => 'member', 'date' => $row['date']);

    }

  }


  # get purchases

  $sql = "SELECT * FROM purchases WHERE date >= '$three_days_ago'";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $notifications_array[] = array('type' => 'purchases', 'date' => $row['date']);

    }

  }




  #borrowed books

  $sql = "SELECT * FROM borrowed_books WHERE timestamp >= '$three_days_ago'";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $notifications_array[] = array('type' => 'borrowed books', 'date' => $row['timestamp']);


    }

  }



  # sort through the latest date

  $notifications = array();

  foreach ($notifications_array as $key => $row){

      $notifications[$key] = $row['date'];

  }

  array_multisort($notifications, SORT_DESC, $notifications_array);




  # print out the details

  for ($i=0; $i < count($notifications_array); $i++) {

    $count++;


    if ($notifications_array[$i]['type'] == 'member') {

      echo "
      <a href='members'>

      <div class='parent'>

        <span>$count</span>

        <li>you have a new member</li>

        <li>".timeDiff($notifications_array[$i]['date'])." ago</li>

      </div></a>";

    }elseif ($notifications_array[$i]['type'] == 'purchases') {

      echo "
      <a href='purchases'>

      <div class='parent'>

        <span>$count</span>

        <li>a new purchase has been made</li>

        <li>".timeDiff($notifications_array[$i]['date'])." ago</li>

      </div>

      </a>";

    }elseif ($notifications_array[$i]['type'] == 'borrowed books') {

      echo "
      <a href='borrowed-books'>

      <div class='parent'>

        <span>$count</span>

        <li>a new book has been borrowed</li>

        <li>".timeDiff($notifications_array[$i]['date'])." ago</li>

      </div>

      </a>";

    }

  }

}



# get counts

function getTableCount($table, $field=0, $clause=0){

  global $conn;

  $sql = "SELECT * FROM $table WHERE $field = '$clause'";

  $result = $conn->query($sql);


  return $result->num_rows;


}






# get account type

function accountType($value){

  if ($value == false) {

    return 'regular account';

  }else {

    return 'member account';

  }

}




# get membership


function getMembership(){

  $library = new CrudFunctionality;

  $library_details = array();





  if (isset($_POST['getmembership'])) {

    # card details

    if($_POST['card'] != ''){

      if (strlen($_POST['card']) < 12) {

        $errors[] = "<p class='error'>card details cant be less than 12 characters</p>";

      }



      if ($_POST['amount'] != '') {

        if ($_POST['amount'] > 1000) {

          $errors[] = "<p class='error'>amount is greater that requested</p>";

        }elseif($_POST['amount'] < 1000) {

          $errors[] = "<p class='error'>ammount is less than requested</p>";

        }else {

          $library_details['email'] = $_COOKIE['user'];

          $library_details['card_no'] = stringShuffle($divide = 4);

        }

      }else {

        $errors[] = "<p class='error'>amount is required</p>";

      }

    }else {

      $errors[] = "<p class='error'>card details are required</p>";

    }



    if (empty($errors)) {


      $return_value = $library->addItems($library_details, 'library_cards');

      if ($return_value != null) {

        echo "<p class='success'>you have purchased a new card</p>";

        header('Location: ../membership');

      }else {

        echo $return_value;

      }

    }else {

      foreach ($errors as $error) {

        echo $error;

      }

    }

  }

}






# function get library_cards

function getLibraryCards($email){

  $count = 0;

  $library = new CrudFunctionality;

  $return_value = $library->getData('*', 'library_cards', 'email', $email);


  if ($return_value != null) {

    echo "
    <div class='parent formal'>

      <span><b>#</b></span>

      <li><b>card id</b></li>

      <li><b>books borrowed</b></li>

      <li><b>expires in</b></li>

    </div>";

    for ($i=0; $i < count($return_value); $i++) {

      $count++;

      echo "
      <div class='parent'>

        <span>$count</span>

        <li>#".strtoupper($return_value[$i]['card_no'])."</li>

        <li>".$return_value[$i]['borrows']."</li>

        <li>".yearSpan($return_value[$i]['date'])."</li>

      </div>";

    }

  }

}





# get all members for admin

function getMembers(){

  $count = 0;

  $members = new CrudFunctionality;



  $return_value = $members->getData('*','users');



  if (gettype($return_value) == 'array') {

    echo "
    <div class='parent formal'>
      <ul>
        <span><b>#</b></span>
        <li><b>name</b></li>
        <li><b>email</b></li>
        <li><b>account type</b></li>
        <li><b>books borrowed</b></li>
      </ul>
    </div>";

    for ($i=0; $i < count($return_value); $i++) {

      $count++;


      echo "<div class='parent'>

        <span>$count</span>

        <li class='cap'>".$return_value[$i]['full_name']."</li>

        <li>".$return_value[$i]['email']."</li>

        <li class='cap'>".accountType(getUserMembership($return_value[$i]['email']))."</li>

        <li>".getTableCount('borrowed_books', 'email', $return_value[$i]['email'] )."</li>

      </div>";

    }

  }

}


# approve and declin epurchases

function userApprovalStatus($email, $code){


  global $conn;

  $sql = "SELECT * FROM purchases WHERE email = '$email' AND code = '$code'";

  $result = $conn->query($sql);

  if($result->num_rows > 0){

    while($row = $result->fetch_assoc()) {

      $status = $row['status'];

    }

  }


  if ($status == 0) {

    return "<p class='right'>waiting for approval</p>";

  }else {

    return "<p class='right'>purchase accepted</p>";

  }

}



# check user member status

function getUserMembership($email){

  $membership = new CrudFunctionality;


  $return_value = $membership->getData('*', 'library_cards', 'email', $email);

  if (gettype($return_value) == 'array') {

    return 1;

  }else {

    return 0;

  }

}





# search functionality

function searchValue(){

  $books = array();

  global $conn;

  $empty = 0;


  if (isset($_POST['search']) && $_POST['search_value'] != '') {

    $index = $_POST['search_value'];



    # first get books

    $sql = "SELECT * FROM books WHERE name LIKE '%".$index."%' OR author LIKE '%".$index."%'";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){

      echo "<p class='success'>Results for <b> $index </b></p>";

      while($row = $result->fetch_assoc()){

        $id = $row['book_id'];

        $name = $row['name'];

        $author = $row['author'];

        $price = $row['price'];

        $count = $row['count'];

        $image = $row['image'];

        # out of stock button output

        if ($count < 1) {

          $value = "<a href='#' disabled class='button'>out of stock</a><br><br>";

          $style = "style='background-color:#999;'";

        }else {
          $value = "<a href='?add_book_to_cart=$id' class='button'> add to cart</a><br><br>";

          $style = null;

        }



        echo "
        <div class='col-sm-1 book_purchase' $style>

          <img src='admin/covers/$image'><br><br>

          <p class='cap'><b>Book Name: </b>$name</p>

          <p class='cap'><b>Author: </b>$author</p>

          <p class='cap'><b>Price: </b>$price Ksh</p>

          <p class='cap'><b>Books Remaining: </b>$count</p><br>

          $value

        </div>";

      }

    }else {

      $empty++;

    }


    $sql = "SELECT * FROM ebooks WHERE name LIKE '%".$index."%' OR author LIKE '%".$index."%'";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){

      while($row = $result->fetch_assoc()){

        $id = $row['ebook_id'];

        $name = $row['name'];

        $author = $row['author'];

        $price = $row['price'];

        $image = $row['image'];

        $size = $row['size'];


        $size = intVal($size / 1000000);



        echo "
        <div class='col-sm-1 book_purchase'>

          <img src='admin/covers/$image'><br><br>

          <p class='cap'><b>Book Name: </b> $name</p>

          <p class='cap'><b>Author: </b> $author</p>

          <p class='cap'><b>Price: </b> $price Ksh</p>

          <p class='cap'><b>Size: </b> $size MB</p><br>

          <a href='?add_ebook_to_cart=$id' class='button'> add to cart</a><br><br>

        </div>";

      }

    }else {

      $empty++;

    }



    if ($empty == 2) {

      echo "<p class='error' style='text-align:center;'>No results for <b>".$index."</b></p>";

    }

  }

}



# function for pending requests

function getPendingRequest($table){

  global $conn;


  if ($table == 'borrowed_books') {

    $sql = "SELECT * FROM $table WHERE status = 0";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){

      echo "<span class='right'>pending requests:".$result->num_rows."</span>";

    }

  }else {

    $sql = "SELECT * FROM $table WHERE status = 0  GROUP BY code";

    $result = $conn->query($sql);

    if ($result->num_rows > 0){

      echo "<span class='right'>pending requests:".$result->num_rows."</span>";

    }

  }

}



 ?>
