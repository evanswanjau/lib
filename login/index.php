<?php require_once '../engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>login</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <style media="screen">
      .container-form{
        padding:10% 0%;
      }
    </style>
  </head>
  <body>
    <div class="container-form">
      <form class="ui-form" action="" method="post">
        <h1>Login</h1>
        <?php loginUser(); ?>
        <input type="text" name="email" placeholder="email"><br><br>
        <input type="password" name="password" placeholder="password"><br><br>
        <input type="submit" name="login" value="login"><br><br>
        <p>Don't have an account? <a href="../signup">sign up</a></p>
      </form>
    </div>
  </body>
</html>
