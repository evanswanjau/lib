<?php

require_once '../../engine/infused_cogs.php';

accountSecurity('../../', 'user');

logOut('../../');

if (!isset($_GET['purchase_code']) || $_GET['purchase_code'] == '') {
  header('Location: ../');
}


# get personal information

$receipt_data = new CrudFunctionality;

$personal_info = $receipt_data->getData('*', 'users', 'email', $_COOKIE['user']);

$personal_info = $receipt_data->getData('*', 'users', 'email', $_COOKIE['user']);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>checkout</title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <style media="screen">
      .book_purchase{
        width:19%!important;
      }
    </style>
  </head>
  <body style="background-color:#b5dcfc;">


    <div class="top-menu">
      <ul>
        <li><a href='../../'>home</a></li>
        <?php echo respectiveMenu('../../'); ?>
        <li><a href='../../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <!-- generate receipt section -->

    <section>
      <div class="container-fluid">

        <div class="row">

          <div class="col-sm-3 side-menu">
            <ul>
              <a href=""><li class="current">profile</li></a>
              <a href="../membership"><li>membership</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>

          <div class="col-sm-9 other-side">

            <div class="receipt">
              <h1>RECEIPT - #<?php echo strtoupper($_GET['purchase_code']); ?> </h1>

              <!-- PERSONAL INFORMATION -->

              <div class="row">
                <div class="step col-sm-12">
                  <h4>personal information</h4>
                  <p class='upper cap'><?php echo $personal_info[0]['full_name']; ?></p>
                  <p><?php echo $personal_info[0]['email']; ?></p>
                </div>
              </div>


            <!--Shopped Items-->
               <div class="row">
                 <div class="step table_design col-sm-12">
                   <h4>shopped books</h4>
                   <table>
                     <tr>
                       <th>#</th>
                       <th>book name</th>
                       <th>author</th>
                       <th>type</th>
                       <th>download</th>
                       <th>price</th>
                     </tr>
                     <?php generateOrderTable($_GET['purchase_code'], $personal_info[0]['email']); ?>
                   </table>
                   <br><br>
                 </div>
               </div>
            </div>

          </div>

        </div>

     </div>

    </section>

  </body>
</html>
