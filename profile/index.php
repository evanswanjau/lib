<?php
require_once '../engine/infused_cogs.php';

accountSecurity('../', 'user');

logOut('../');

# get personal information

$user_data = new CrudFunctionality;

$personal_info = $user_data->getData('*', 'users', 'email', $_COOKIE['user']);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>profile</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  </head>
  <body>

    <div class="top-menu">
      <ul>
        <li><a href='../'>home</a></li>
        <?php echo respectiveMenu('../'); ?>
        <li><a href='../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <section>
      <div class="container-fluid">
        <div class="row">

          <div class="col-sm-3 side-menu">
            <ul>
              <a href=""><li class="current">profile</li></a>
              <a href="membership"><li>membership</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>

          <div class="col-sm-9 other-side">
            <br><br>
            <h3>My profile</h3>

            <!-- personal information -->

            <div class="row">
              <div class="step col-sm-10">
                <h4>personal information</h4>
                <br>
                <p class='upper cap'><b>Name: </b><?php echo $personal_info[0]['full_name']; ?></p>
                <p><b>Email: </b><?php echo $personal_info[0]['email']; ?></p>

                <?php if (!empty($personal_info[0]['status'])): ?>
                  <p class="cap"><b>Account: </b><?php echo accountType($personal_info[0]['status']); ?></p>
                <?php endif; ?>
              </div>
            </div>


            <!-- borrowed books -->

            <div class="row">
              <div class="step col-sm-10">
                <h4>borrowed books</h4>
                <br>
                <?php getUserBorrowedBooks($personal_info[0]['email']) ?>
              </div>
            </div>


            <!-- purchases -->
            <div class="row">
              <div class="step col-sm-10">
                <h4>purchases</h4>
                <br>
                <?php getUserOrders($personal_info[0]['email']); ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

  </body>
</html>
