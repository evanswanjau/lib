<?php require_once '../../engine/infused_cogs.php'; accountSecurity('../../', 'user'); logOut('../../');?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>membership</title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body style="background-color:#b5dcfc;">


    <div class="top-menu">
      <ul>
        <li><a href='../../'>home</a></li>
        <?php echo respectiveMenu('../../'); ?>
        <li><a href='../../cart'>cart(<?php getValue(); ?>)</a></li>
      </ul>
    </div>

    <!-- generate receipt section -->

    <section>
      <div class="container-fluid">

        <div class="row">

          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>profile</li></a>
              <a href="../membership"><li class="current">membership</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>

          <div class="col-sm-9 other-side">
            <br><br>

            <?php if (getTableCount('library_cards', 'email', $_COOKIE['user']) < 5): ?>

              <h3>Membership</h3>
              <br><br>
              <form class="ui-form" action="" method="post">
                <?php getMembership(); ?>
                <p style="color:#333;">Pay 1000 ksh annual fee for a card</p>
                <input type="text" name="card" placeholder="xxxx - xxxx- xxxx - xxxx"><br><br>
                <input type="text" name="amount" value="1000"><br><br>
                <input type="submit" name="getmembership" value="get membership">
              </form>

            <?php endif; ?>


            <h3>Library Cards</h3>
            <?php getLibraryCards($_COOKIE['user']); ?>

          </div>

        </div>

     </div>

    </section>

  </body>
</html>
