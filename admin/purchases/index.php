<?php require_once '../../engine/infused_cogs.php'; accountSecurity('../../', 'admin'); logOut('../../'); $sql = "UPDATE purchases SET status = 1";?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>ebooks</title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li>books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li class="current">purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <h3>Purchases</h3>
            <div class="row">
              <?php adminPurchases(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
