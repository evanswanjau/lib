<?php require_once '../../engine/infused_cogs.php'; accountSecurity('../../', 'admin');; logOut('../../'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li>books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li class="current">members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <h3>Members</h3>
            <?php getMembers(); ?>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
