<?php require_once '../engine/infused_cogs.php'; accountSecurity('../', 'admin');; logOut('../'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href=""><li class="current">dashboard</li></a>
              <a href="borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="books"><li>books</li></a>
              <a href="ebooks"><li>ebooks</li></a>
              <a href="purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <div class="col-sm-11 stats">
              <div class="row">
                <ul>
                  <li>members: <?php echo getTableCount('users'); ?></li>
                  <li>borrowed books: <?php echo getTableCount('borrowed_books'); ?></li>
                  <li>purchases: <?php echo getTableCount('purchases'); ?></li>
                  <li>books: <?php echo getTableCount('books'); ?></li>
                  <li>ebooks: <?php echo getTableCount('ebooks'); ?></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-11 notifications">
              <h3>Notifications</h3>
              <div class="parent formal">
                <ul>
                  <span>#</span>
                  <li>message</li>
                  <li>received</li>
                </ul>
              </div>
              <?php getAdminNotifications(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
