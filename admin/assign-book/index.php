<?php

require_once '../../engine/infused_cogs.php';

accountSecurity('../../', 'admin');
logOut('../../');

if (isset($_GET['assign_book_id']) && $_GET['assign_book_id'] != '') {

  $id = $_GET['assign_book_id'];

  $sql = "SELECT * FROM books WHERE book_id = $id";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()) {
    $name = $row['name'];
    $author = $row['author'];
    $price = $row['price'];
    $count = $row['count'];
  }
}else {
  $id = null;
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>assign book</title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li class="current">books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <h3>assign book</h3>
            <br><br>
            <form class="ui-form" action="" method="get">

              <?php if (isset($_GET['username']) && $_GET['username'] != ''): ?>

                <?php getUserData(clean_data($_GET['username']), $id); ?>

              <?php else: ?>

                <?php if (isset($_GET['assign_lib_id']) && $_GET['assign_lib_id'] != ''): ?>

                  <?php assignBook($_GET['assign_book_id'], $_GET['assign_lib_id'], $_GET['email']); ?>

                <?php else: ?>

                  <p class="error">please enter card no</p>

                <?php endif; ?>



              <?php endif; ?>

              <input type="hidden" name="assign_book_id" value="<?php echo $id; ?>">
              <input type="text" name="name" placeholder="book name" value="<?php if (empty($name)) {echo null;}else {echo $name;}?>"><br>
              <input type="text" name="author" placeholder="author" value="<?php if (empty($author)) {echo null;}else {echo $author;}?>"><br>
              <input type="text" name="username" placeholder="member email or library id"><br><br>
              <input type="submit" name="assign-book" value="assign book">
            </form>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
