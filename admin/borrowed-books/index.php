<?php require_once '../../engine/infused_cogs.php'; accountSecurity('../../', 'admin');; logOut('../../'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <style media="screen">
      .parent{
        padding-top:1%;
        padding-bottom:.5%;
      }
    </style>
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li class="current">borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li>books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <?php approveBorrow(); ?>
            <h3>Borrowed Books</h3>
            <?php if (isset($_GET['return_book_id']) && $_GET['return_book_id'] != ''): ?>

              <?php returnBook($_GET['return_borrow_id'], $_GET['return_book_id'], $_GET['card_no']); ?>

            <?php endif; ?>

            <?php getBorrowedBooks(); ?>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
