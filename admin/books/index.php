<?php require_once '../../engine/infused_cogs.php'; accountSecurity('../../', 'admin'); logOut('../../'); deleteBook();?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>books</title>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li class="current">books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <h3>books</h3>
            <a href="../add-book" class="button right">add new book</a>
            <br><br><br>
            <div class="row">
              <?php getBooks(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
