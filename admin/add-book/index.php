<?php

require_once '../../engine/infused_cogs.php';

accountSecurity('../../', 'admin');
logOut('../../');

if (isset($_GET['edit_book_id']) && $_GET['edit_book_id'] != '') {

  $id = $_GET['edit_book_id'];

  $sql = "SELECT * FROM books WHERE book_id = $id";
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()) {
    $name = $row['name'];
    $author = $row['author'];
    $price = $row['price'];
    $count = $row['count'];
  }
}else {
  $id = null;
}

 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php if (isset($_GET['edit_book_id']) && $_GET['edit_book_id'] != ''): ?>
      <title>edit book</title>
    <?php else: ?>
      <title>add book</title>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
  </head>
  <body>

    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 side-menu">
            <ul>
              <a href="../"><li>dashboard</li></a>
              <a href="../borrowed-books"><li>borrowed books <?php getPendingRequest('borrowed_books'); ?></li></a>
              <a href="../books"><li class="current">books</li></a>
              <a href="../ebooks"><li>ebooks</li></a>
              <a href="../purchases"><li>purchases <?php getPendingRequest('purchases'); ?></li></a>
              <a href="../members"><li>members</li></a>
              <a href="?logout"><li>logout</li></a>
            </ul>
          </div>
          <div class="col-sm-9 other-side">
            <?php if (isset($_GET['edit_book_id']) && $_GET['edit_book_id'] != ''): ?>
              <h3>edit book</h3>
            <?php else: ?>
              <h3>edit book</h3>
            <?php endif; ?>
            <br><br>
            <form class="ui-form" action="" method="post" enctype="multipart/form-data">
              <?php addNewBook(); editBook($id);?>
              <input type="text" name="name" placeholder="book name" value="<?php if (empty($name)) {echo null;}else {echo $name;}?>"><br>
              <input type="text" name="author" placeholder="author" value="<?php if (empty($author)) {echo null;}else {echo $author;}?>"><br>
              <input type="text" name="price" placeholder="price" value="<?php if (empty($price)) {echo null;}else {echo $price;}?>"><br>
              <input type="number" name="count" placeholder="count" value="<?php if (empty($count)) {echo null;}else {echo $count;}?>"><br>
              <p>add book cover</p>
              <input type="file" name="image"><br>
              <?php if (isset($_GET['edit_book_id']) && $_GET['edit_book_id'] != ''): ?>
                <input type="submit" name="edit-book" value="edit book">
              <?php else: ?>
                <input type="submit" name="add-book" value="add book">
              <?php endif; ?>
            </form>
          </div>
        </div>
      </div>
    </section>

  </body>
</html>
