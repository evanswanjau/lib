<?php require_once '../engine/infused_cogs.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>sign up</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <style media="screen">
      .container-form{
        padding:5% 0%;
      }
    </style>
  </head>
  <body>
    <div class="container-form">
      <form class="ui-form" action="" method="post">
        <h1>Register</h1>
        <?php signUp(); ?>
        <input type="text" name="full_name" placeholder="full name"><br><br>
        <input type="email" name="email" placeholder="email"><br><br>
        <input type="text" name="phone" placeholder="phone number"><br><br>
        <input type="text" name="location" placeholder="location"><br><br>
        <input type="password" name="password" placeholder="password"><br><br>
        <input type="password" name="password2" placeholder="confirm password"><br><br>

        <input type="submit" name="signup" value="sign up"><br><br>
        <p>Already have an account?<a href="../login"> login</a></p>
      </form>
    </div>
  </body>
</html>
