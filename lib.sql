-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2018 at 08:23 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lib`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(2, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `author` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `name`, `author`, `price`, `count`, `image`) VALUES
(1, 'Tess of the roa', 'rachel hartman', 600, 5, 'ar0q1bcksl.jpg'),
(2, 'diary of a wimpy kid', 'jeff kinney', 500, 5, 'e0asl9n8d4.jpg'),
(4, 'dracula', 'bram stoker', 100, 5, 'qag1s0j3pm.jpg'),
(5, 'the room', 'jonas karlsson', 700, 5, 'ab3kn251ih.jpg'),
(7, 'fourth wind', 'quinn whitfield', 800, 5, 'default.png'),
(8, 'river between', 'ngugi wa thiongo', 400, 5, 'default.png'),
(9, 'animal farm', 'george orwell', 1000, 5, '0bngk6thm3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `borrowed_books`
--

CREATE TABLE IF NOT EXISTS `borrowed_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `card_no` varchar(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due_date` timestamp NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `ebooks`
--

CREATE TABLE IF NOT EXISTS `ebooks` (
  `ebook_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `author` varchar(25) NOT NULL,
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `image` varchar(100) NOT NULL,
  `ebook` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  PRIMARY KEY (`ebook_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ebooks`
--

INSERT INTO `ebooks` (`ebook_id`, `name`, `author`, `price`, `count`, `image`, `ebook`, `size`) VALUES
(1, 'the martian', 'andy weir', 1000, 1, 'qmk753ij9l.jpg', 'hcs62a08n7.pdf', 2169231),
(2, 'the vegetarian', 'han kang', 2000, 1, '6trc0j2oqk.png', '32on7bcqk4.pdf', 2169231),
(3, 'the smallest thing', 'lisa manterfield', 800, 1, '5np4ic0q2e.jpg', '270n4qf5k9.pdf', 2169231);

-- --------------------------------------------------------

--
-- Table structure for table `library_cards`
--

CREATE TABLE IF NOT EXISTS `library_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_no` varchar(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `borrows` int(11) DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `library_cards`
--

INSERT INTO `library_cards` (`id`, `card_no`, `email`, `borrows`, `date`) VALUES
(6, '5ph10a4', 'mary@gmail.com', 0, '2018-08-09 08:46:23'),
(7, 'm6ask9e', 'mary@gmail.com', 0, '2018-08-09 08:46:48'),
(8, 'eiojk2p', 'mary@gmail.com', 0, '2018-08-09 08:46:52'),
(9, 'a8sm3np', 'mary@gmail.com', 0, '2018-08-09 08:46:55'),
(10, '5n148bs', 'mary@gmail.com', 0, '2018-08-09 08:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `code` varchar(15) NOT NULL,
  `type` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`purchase_id`, `book_id`, `code`, `type`, `email`, `date`, `status`, `delivery`) VALUES
(1, 4, '74fmdcj', 'book', 'adam@gmail.com', '2018-08-03 09:07:11', 0, 0),
(2, 3, '74fmdcj', 'book', 'adam@gmail.com', '2018-08-03 09:07:12', 0, 0),
(3, 2, '74fmdcj', 'book', 'adam@gmail.com', '2018-08-03 09:07:12', 0, 0),
(4, 4, '629cojr', 'book', 'telvin@gmail.com', '2018-08-03 09:56:22', 0, 0),
(5, 5, '629cojr', 'book', 'telvin@gmail.com', '2018-08-03 09:56:22', 0, 0),
(9, 2, 't3s7q8i', 'book', 'mary@gmail.com', '2018-08-09 08:56:56', 0, 0),
(10, 1, 't3s7q8i', 'book', 'mary@gmail.com', '2018-08-09 08:56:56', 0, 0),
(11, 4, 't3s7q8i', 'book', 'mary@gmail.com', '2018-08-09 08:56:56', 0, 0),
(12, 2, 't3s7q8i', 'ebook', 'mary@gmail.com', '2018-08-09 08:56:56', 0, 0),
(13, 1, 't3s7q8i', 'ebook', 'mary@gmail.com', '2018-08-09 08:56:56', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `location` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `full_name`, `email`, `phone`, `location`, `password`, `status`, `date`) VALUES
(2, 'mary wanjiku', 'mary@gmail.com', '0789345623', 'thika', '5f4dcc3b5aa765d61d8327deb882cf99', 0, '2018-08-09 08:44:45');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
